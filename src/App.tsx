import { Col, Container, Row, Stack } from 'react-bootstrap';
import TodoList from './components/TodoList';
import TodoCreate from './components/TodoCreate';

const App = () => {
  return (
    <Container>
      <Row className="mt-3">
        <Col>
          <Stack gap={3} className="align-items-center my-5">
            <h1>TODO list</h1>
            <TodoCreate />
            <TodoList />
          </Stack>
        </Col>
      </Row>
    </Container>
  );
};

export default App;
