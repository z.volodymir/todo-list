import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { Modal } from 'types';

const initialState: Modal = {
  isOpen: false,
  todoId: null,
};

const modalSlice = createSlice({
  name: '@@modal',
  initialState,
  reducers: {
    toggleModal: (state, action: PayloadAction<Modal['isOpen']>) => {
      state.isOpen = action.payload;
    },
    setTodoId: (state, action: PayloadAction<Modal['todoId']>) => {
      state.todoId = action.payload;
    },
  },
});

export const { toggleModal, setTodoId } = modalSlice.actions;

export const modalReducer = modalSlice.reducer;
