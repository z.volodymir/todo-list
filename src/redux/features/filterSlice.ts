import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { Filter } from 'types';

const initialState: Filter = 'all';

const filterSlice = createSlice({
  name: '@@filter',
  initialState,
  reducers: {
    setFilter: (_, action: PayloadAction<Filter>) => action.payload,
  },
});

export const { setFilter } = filterSlice.actions;

export const filterReducer = filterSlice.reducer;
