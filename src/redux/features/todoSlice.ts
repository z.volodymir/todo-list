import { PayloadAction, createSlice } from '@reduxjs/toolkit';
import { Todo } from '../../types';

type TodoId = Todo['id'];

const initialState: Todo[] = [
  { id: 1, title: 'React', description: 'Learn React', isCompleted: false },
  { id: 2, title: 'Redux', description: 'Learn Redux', isCompleted: true },
];

const todoSlice = createSlice({
  name: '@@todos',
  initialState,
  reducers: {
    addTodo: (state, action: PayloadAction<Todo>) => {
      state.push(action.payload);
    },
    deleteTodo: (state, action: PayloadAction<TodoId>) => {
      return state.filter((todo) => todo.id !== action.payload);
    },
    setCompleted: (state, action: PayloadAction<TodoId>) => {
      const todo = state.find((todo) => todo.id === action.payload);

      if (todo) {
        todo.isCompleted = !todo.isCompleted;
      }
    },
    editTodo: (state, action: PayloadAction<Todo>) => {
      const todo = state.find((todo) => todo.id === action.payload.id);

      if (todo) {
        todo.title = action.payload.title;
        todo.description = action.payload.description;
        todo.isCompleted = action.payload.isCompleted;
      }
    },
  },
});

export const { addTodo, deleteTodo, setCompleted, editTodo } =
  todoSlice.actions;

export const todoReducer = todoSlice.reducer;
