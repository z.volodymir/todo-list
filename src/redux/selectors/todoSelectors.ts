import { createSelector } from '@reduxjs/toolkit';
import { selectFilter } from './filterSelectors';
import { RootState } from 'redux/store';
import { Todo } from 'types';

export const selectTodos = (state: RootState) => state?.todos;

export const selectFoundTodo = (id: Todo['id']) =>
  createSelector(selectTodos, (todos) => todos.find((todo) => todo.id === id));

export const selectFilteredTodos = createSelector(
  [selectTodos, selectFilter],
  (allTodos, activeFilter) => {
    switch (activeFilter) {
      case 'all':
        return allTodos;
      case 'completed':
        return allTodos.filter((todo) => todo.isCompleted);
      case 'active':
        return allTodos.filter((todo) => !todo.isCompleted);
      default:
        return allTodos;
    }
  }
);
