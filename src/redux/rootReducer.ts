import { combineReducers } from 'redux';
import { todoReducer } from './features/todoSlice';
import { modalReducer } from './features/modalSlice';
import { filterReducer } from './features/filterSlice';

export const rootReducer = combineReducers({
  todos: todoReducer,
  filter: filterReducer,
  modal: modalReducer,
});
