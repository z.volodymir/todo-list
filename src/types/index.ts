export type Todo = {
  id: number;
  title: string;
  description: string;
  isCompleted: boolean;
};

export type Modal = {
  isOpen: boolean;
  todoId: number | null;
};

export type Filter = string;
