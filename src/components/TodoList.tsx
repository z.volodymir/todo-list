import { ListGroup } from 'react-bootstrap';
import { useAppSelector } from 'redux/redux-hooks';
import { selectFilteredTodos } from 'redux/selectors/todoSelectors';
import TodoItem from './TodoItem';
import TodoFilter from './TodoFilter';

const TodoList = () => {
  const filteredTodos = useAppSelector(selectFilteredTodos);

  return (
    <>
      <TodoFilter />
      <ListGroup as="ul" className="w-100">
        {filteredTodos.length ? (
          filteredTodos.map(({ id, title, description, isCompleted }) => (
            <TodoItem
              key={id}
              id={id}
              title={title}
              description={description}
              isCompleted={isCompleted}
            />
          ))
        ) : (
          <h3 className="text-center">No tasks</h3>
        )}
      </ListGroup>
    </>
  );
};

export default TodoList;
