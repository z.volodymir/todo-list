import { Button, ButtonGroup } from 'react-bootstrap';
import { useAppDispatch } from 'redux/redux-hooks';
import { setFilter } from 'redux/features/filterSlice';

const TodoFilter = () => {
  const dispatch = useAppDispatch();

  return (
    <ButtonGroup>
      <Button variant="outline-dark" onClick={() => dispatch(setFilter('all'))}>
        Show all
      </Button>
      <Button variant="primary" onClick={() => dispatch(setFilter('active'))}>
        Show active
      </Button>
      <Button
        variant="success"
        onClick={() => dispatch(setFilter('completed'))}
      >
        Show completed
      </Button>
    </ButtonGroup>
  );
};

export default TodoFilter;
