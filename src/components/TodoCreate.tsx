import { Button } from 'react-bootstrap';
import { useAppDispatch } from 'redux/redux-hooks';
import { setTodoId, toggleModal } from 'redux/features/modalSlice';
import TodoModal from './TodoModal';
import TodoForm from './TodoForm';

const TodoCreate = () => {
  const dispatch = useAppDispatch();

  const openCreateModal = () => {
    dispatch(toggleModal(true));
    dispatch(setTodoId(null));
  };

  return (
    <>
      <Button variant="dark" onClick={openCreateModal}>
        Create task
      </Button>
      <TodoModal>
        <TodoForm />
      </TodoModal>
    </>
  );
};

export default TodoCreate;
