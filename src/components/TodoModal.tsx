import { ReactNode } from 'react';
import { Modal } from 'react-bootstrap';
import { useAppDispatch, useAppSelector } from 'redux/redux-hooks';
import { selectModal } from '../redux/selectors/modalSelectors';
import { toggleModal } from '../redux/features/modalSlice';

interface Props {
  children: ReactNode;
}

const TodoModal = ({ children }: Props) => {
  const { isOpen } = useAppSelector(selectModal);
  const dispatch = useAppDispatch();

  return (
    <Modal show={isOpen} onHide={() => dispatch(toggleModal(false))}>
      <Modal.Header closeButton>
        <Modal.Title>Task form</Modal.Title>
      </Modal.Header>
      <Modal.Body>{children}</Modal.Body>
    </Modal>
  );
};

export default TodoModal;
