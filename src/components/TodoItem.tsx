import React from 'react';
import {
  ListGroupItem,
  Stack,
  Badge,
  ButtonGroup,
  Button,
} from 'react-bootstrap';
import { useAppDispatch } from 'redux/redux-hooks';
import { deleteTodo, setCompleted } from 'redux/features/todoSlice';
import { setTodoId, toggleModal } from 'redux/features/modalSlice';
import { Todo } from 'types';

const TodoItem = React.memo(({ id, title, description, isCompleted }: Todo) => {
  const dispatch = useAppDispatch();

  const openEditModal = (id: Todo['id']) => {
    dispatch(toggleModal(true));
    dispatch(setTodoId(id));
  };

  return (
    <ListGroupItem as="li" className="w-100 p-3">
      <Stack gap={3} className="align-items-start">
        <Badge
          pill
          bg={isCompleted ? 'success' : 'primary'}
          className="text-uppercase"
        >
          {isCompleted ? 'completed' : 'active'}
        </Badge>
        <h3
          className={isCompleted ? 'text-decoration-line-through m-0' : 'm-0'}
        >
          {title}
        </h3>
        <p className="m-0">{description}</p>
        <ButtonGroup>
          <Button variant="success" onClick={() => dispatch(setCompleted(id))}>
            Complete
          </Button>
          <Button variant="info" onClick={() => openEditModal(id)}>
            Edit
          </Button>
          <Button variant="danger" onClick={() => dispatch(deleteTodo(id))}>
            Delete
          </Button>
        </ButtonGroup>
      </Stack>
    </ListGroupItem>
  );
});

export default TodoItem;
