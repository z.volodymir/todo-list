import { useForm } from 'react-hook-form';
import {
  Button,
  ButtonGroup,
  Form,
  FormCheck,
  FormControl,
  FormGroup,
  Stack,
} from 'react-bootstrap';
import { useAppDispatch, useAppSelector } from 'redux/redux-hooks';
import { selectFoundTodo } from 'redux/selectors/todoSelectors';
import { selectModal } from 'redux/selectors/modalSelectors';
import { toggleModal } from 'redux/features/modalSlice';
import { addTodo, editTodo } from 'redux/features/todoSlice';
import { Todo } from 'types';

const TodoForm = () => {
  const { todoId } = useAppSelector(selectModal);
  const todo = useAppSelector(selectFoundTodo(todoId as Todo['id']));
  const dispatch = useAppDispatch();

  const {
    handleSubmit,
    register,
    formState: { errors },
  } = useForm({
    defaultValues: {
      isCompleted: todo ? todo.isCompleted : false,
      title: todo ? todo.title : '',
      description: todo ? todo.description : '',
    },
  });

  const onSubmit = (data: Omit<Todo, 'id'>) => {
    if (todoId) {
      dispatch(
        editTodo({
          id: todoId,
          ...data,
        })
      );
    } else {
      dispatch(
        addTodo({
          id: Date.now(),
          ...data,
        })
      );
    }

    dispatch(toggleModal(false));
  };

  return (
    <Form noValidate onSubmit={handleSubmit(onSubmit)}>
      <Stack gap={3}>
        <FormGroup controlId="validationFormik01">
          <FormControl
            type="text"
            placeholder="Task title"
            required
            {...register('title', {
              required: 'Please provide a valid title.',
              pattern: /^[A-Za-z А-ЯЁ]+/i,
            })}
            name="title"
            aria-invalid={errors.title ? 'true' : 'false'}
          />

          {errors?.title && (
            <FormControl.Feedback type="invalid" className="d-block">
              {errors.title.message}
            </FormControl.Feedback>
          )}
        </FormGroup>
        <FormGroup>
          <FormControl
            as="textarea"
            placeholder="Task description"
            required
            {...register('description', {
              required: 'Please provide a valid description.',
              pattern: /^[A-Za-z А-ЯЁ]+/i,
            })}
            name="description"
            aria-invalid={errors.description ? 'true' : 'false'}
          />

          {errors?.description && (
            <FormControl.Feedback type="invalid" className="d-block">
              {errors.description.message}
            </FormControl.Feedback>
          )}
        </FormGroup>
        <FormGroup>
          <FormCheck
            type="checkbox"
            required
            label="Completed"
            {...register('isCompleted')}
          />
        </FormGroup>
        <ButtonGroup>
          <Button
            variant="secondary"
            onClick={() => dispatch(toggleModal(false))}
          >
            Close
          </Button>
          <Button type="submit" variant="primary">
            Save
          </Button>
        </ButtonGroup>
      </Stack>
    </Form>
  );
};

export default TodoForm;
